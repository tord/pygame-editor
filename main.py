import csv
import enum
import logging
import random
import sys
import os
import os.path
import functools
import xml.etree.ElementTree
import pygame
import pygame.locals
import input_control


CELL_SIZE = 32
NR_CELLS_X = 20  # -visible on the screen
NR_CELLS_Y = 15
NR_PX_X = NR_CELLS_X * CELL_SIZE  # -visible on the screen
NR_PX_Y = NR_CELLS_Y * CELL_SIZE
INIT_WIN_WIDTH_PX = NR_PX_X
INIT_WIN_HEIGHT_PX = NR_PX_Y
NORMAL_WIN_SIZE = (INIT_WIN_WIDTH_PX, INIT_WIN_HEIGHT_PX)

pygame.init()
WIN_FLAGS_NORMAL = pygame.locals.SCALED
WIN_FLAGS_WITH_FULLSCREEN = WIN_FLAGS_NORMAL | pygame.locals.FULLSCREEN
# More flags: DOUBLEBUF, HWSURFACE, RESIZABLE

GRAY = (128, 128, 128)
WHITE = (225, 225, 225)
BLACK = (30, 30, 30)
GREEN = (30, 230, 30)
DARK_GREEN = (0, 120, 0)
BROWN = (100, 40, 40)


"""
class GameState(enum.Enum):
    setup = enum.auto()
    moving = enum.auto()
    meditating = enum.auto()
"""


class PlayerState(enum.Enum):
    moving = enum.auto()
    meditating = enum.auto()


class AreaType(enum.Enum):
    not_set = enum.auto()
    forest = enum.auto()
    village = enum.auto()


class Area:
    area_dict = {}
    active = None

    def __init__(self, i_filename, i_music_file: str = ""):
        if i_filename in Area.area_dict.keys():
            print("WARNING: This map has already been loaded")
        self.map_file: str = i_filename
        self.music_file: str = i_music_file
        self.area_type = AreaType.not_set
        self.map_matrix = None
        # tileset attribute? connected to area_type?

        Area.area_dict[i_filename] = self

    def load_from_lvl(self):
        # Alternatively using a dict with (x, y) tuple as keys
        # Old: ret_level_matrix = [[None for _ in range(NR_CELLS_X)] for _ in range(NR_CELLS_Y)]
        ret_level_matrix = []
        line_length_list = []
        with open(self.map_file, "r") as file:
            line_list = file.readlines()
            print(f"{len(line_list)=}")
            for y, line_str in enumerate(line_list):
                line_str = line_str.strip()
                line_length = len(line_str)
                line_length_list.append(line_length)
                tmp_level_line_list = []
                for x, char_str in enumerate(line_str):
                    cell_object = None
                    if char_str == '@':
                        global player_cell
                        player_cell = Player(x, y, controller)
                    elif char_str != ' ':
                        cell_class = object_representations_dict[char_str]
                        cell_object = cell_class(x, y)
                    # ret_level_matrix[y][x] = cell_object
                    tmp_level_line_list.append(cell_object)
                ret_level_matrix.append(tmp_level_line_list)
        print(f"{len(ret_level_matrix)=}")
        print(f"{max(line_length_list)=}")
        line_length_set = set(line_length_list)
        if len(line_length_set) > 1:
            print("WARNING! Line length differs between different lines")
        print(f"{line_length_set=}")
        return ret_level_matrix

    def load_from_tmx(self):
        ret_level_matrix = []
        line_length_list = []
        tree = xml.etree.ElementTree.parse(self.map_file)
        root = tree.getroot()

        layer = tree.find("layer")
        data_element = layer.find("data")
        # -please note that "find" only works on direct child nodes (not grandchildren)
        csv_data = data_element.text
        line_list = csv_data.split('\n')
        csv_reader = csv.reader(line_list)
        for row in csv_reader:
            if row:  # -last row is empty
                print(f"{row=}")
        # result = re.search(r'<data encoding="csv">(.*)</data>', contents)
        # csv_contents = result.group(1)
        # xml.sax.
        # https://docs.python.org/3/library/xml.html

        return ret_level_matrix

    def set_as_active(self):
        # fade out music for previous
        # pygame.mixer.music.fadeout(5000)  # - this is blocking!

        if self.map_file.endswith(".lvl"):
            self.map_matrix = self.load_from_lvl()
        elif self.map_file.endswith(".tmx"):
            self.map_matrix = self.load_from_tmx()

        Area.active = self

        # play new music (looping)
        if self.music_file:
            pygame.mixer.music.load(self.music_file)
            pygame.mixer.music.play(-1)  # -fade_ms=3000
            # pygame.mixer.music.set_endevent()
            # pygame.mixer.music.queue()


class Camera:
    x = 0  # -relative to the level
    y = 0

    @staticmethod
    def get_rect():
        ret_rect = pygame.Rect(Camera.x, Camera.y, NR_PX_X, NR_PX_Y)
        return ret_rect

    @staticmethod
    def set_pos(i_x, i_y):
        Camera.x = i_x
        Camera.y = i_y


def cell_to_px_coords(i_x_cell: int, i_y_cell: int):
    # cell coords start at 0
    px_x = CELL_SIZE * i_x_cell
    px_y = CELL_SIZE * i_y_cell
    return (px_x, px_y)


def px_to_cell_coords(i_x_px: int, i_y_px: int):
    cell_x = i_x_px // CELL_SIZE
    cell_y = i_y_px // CELL_SIZE
    return (cell_x, cell_y)


def get_files(i_dir, i_prefix, i_suffix):
    file_list = os.listdir(i_dir)
    ret_list = []
    for filename in file_list:
        if filename.startswith(i_prefix) and filename.endswith(i_suffix):
            relative_file_path = os.path.join(i_dir, filename)
            ret_list.append(relative_file_path)
    return ret_list


class GameObject():
    def __init__(self, i_xpx: int, i_ypx, i_width: int, i_height: int):
        self.map_x_px = i_xpx  # -to be adjusted by the camera position when drawing
        self.map_y_px = i_ypx  # - -"-
        self.width = i_width
        self.height = i_height
        self.color_te = WHITE
        self.image_surface = None
        self.when_entered_func = None

    """
    def when_entered(self):
        pass
    """

    def get_map_rect(self):
        ret_rect = pygame.Rect(self.map_x_px, self.map_y_px, self.width, self.height)
        return ret_rect

    def get_draw_rect(self):
        draw_x = self.map_x_px - Camera.x
        draw_y = self.map_y_px - Camera.y
        ret_rect = pygame.Rect(draw_x, draw_y, self.width, self.height)
        return ret_rect

    def draw(self, i_fake_surface):
        draw_rect = self.get_draw_rect()
        if self.image_surface is not None:
            # Scale functions: transform .scale, .scale2x, .smoothscale
            # Please note that these give a different result and might depend on hardware:
            # * scale and scale2x: different on HP laptop (i5)
            # * scale and smoothscale: different on ASUS laptop
            i_fake_surface.blit(self.image_surface, (draw_rect.x, draw_rect.y))
        else:
            pygame.draw.rect(i_fake_surface, self.color_te, draw_rect)


class GridObject(GameObject):
    def __init__(self, i_xc: int, i_yc: int, i_cell_width: int = 1, i_cell_height: int = 1):
        xpx = CELL_SIZE * i_xc
        ypx = CELL_SIZE * (i_yc - i_cell_height + 1)
        # TODO: We may want to update this so that images larger than 32 px
        #  (but still just one cell) are drawn more above instead of more below
        super().__init__(xpx, ypx, i_cell_width * CELL_SIZE, i_cell_height * CELL_SIZE)
        self.color_te = GRAY
        self.is_blocking = False


class WallCell(GridObject):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.color_te = GREEN
        self.is_blocking = True


class GroundCell(GridObject):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.image_surface = pygame.image.load("img/grass-1_32x32.png")


class MeditatingMonkCell(GridObject):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.image_surface = pygame.image.load("img/meditating-monk.png")
        self.is_blocking = True


class TreeCell(GridObject):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        file_name_list = get_files("img", "tree-", ".png")
        file_name = random.choice(file_name_list)
        self.image_surface = pygame.image.load(file_name)
        self.is_blocking = True


class LargeTree(GridObject):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc, 3, 3)
        self.image_surface = pygame.image.load("img/large-tree-1_rescaled-96x96.png")
        # self.color_te = BROWN
        self.is_blocking = True


def switch_to_new_map(i_file_name: str):
    levels_dir: str = "levels"  # -TODO: Making this absolute
    file_path: str = os.path.join(levels_dir, i_file_name)
    new_area = Area(file_path)
    # "levels/level-2-20x15.lvl"
    new_area.set_as_active()


class ExitCell(GridObject):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.color_te = GRAY
        self.when_entered_func = functools.partial(switch_to_new_map, "level-2-20x15.lvl")


class Player(GridObject):
    def __init__(self, i_start_xc: int, i_start_yc: int, i_controller):
        super().__init__(i_start_xc, i_start_yc)
        self.color_te = WHITE
        self.controller = i_controller
        self.speed = 1  # -please note that if changing the speed from 1, we run the risk of the collisions not working
        self.breathing_width = 100
        self.state = PlayerState.moving

    def update(self, i_level_matrix, i_btn_just_pressed: input_control.ButtonPressed) -> tuple[int, int]:
        # sourcery skip: merge-else-if-into-elif
        pr = player_cell.get_map_rect()

        if self.state == PlayerState.moving:
            if i_btn_just_pressed == input_control.ButtonPressed.btn_b:
                # if self.controller.btn_b:
                self.state = PlayerState.meditating
            else:
                slow_down = 1.0
                if self.controller.btn_a:
                    slow_down = 0.2

                dx, dy = self.controller.get_delta_pos()
                self.map_x_px += dx * self.speed * slow_down
                self.map_y_px += dy * self.speed * slow_down

                collision_cell_list = []

                for i, level_list in enumerate(i_level_matrix):
                    for j, cell_obj in enumerate(level_list):
                        cell_obj: GridObject
                        if cell_obj and cell_obj.is_blocking:
                            cr = cell_obj.get_map_rect()
                            pr = player_cell.get_map_rect()
                            if pr.colliderect(cr):
                                # print(f"cr collision {i}, {j}, pixels: {cell_obj.x} {cell_obj.y}")
                                collision_cell_list.append(cell_obj)

                x_when_blocked = self.map_x_px
                y_when_blocked = self.map_y_px
                left_right_count = 0
                up_down_count = 0
                for collision_cell in collision_cell_list:
                    """
                    if dx < 0:
                        x_when_blocked = cr.right + 0
                        left_right_count += 1
                    if dx > 0:
                        x_when_blocked = cr.left - player_cell.width - 0
                        left_right_count += 1
                    if dy < 0:
                        y_when_blocked = cr.bottom + 0
                        up_down_count += 1
                    if dy > 0:
                        y_when_blocked = cr.top - player_cell.height - 0
                        up_down_count += 1
                    """
                    cr = collision_cell.get_map_rect()
                    if dx < 0 and abs(cr.right - pr.left) <= 3 * self.speed:
                        x_when_blocked = cr.right + 0
                        left_right_count += 1
                    if dx > 0 and abs(pr.right - cr.left) <= 3 * self.speed:
                        x_when_blocked = cr.left - player_cell.width - 0
                        left_right_count += 1
                    if dy < 0 and abs(cr.bottom - pr.top) <= 3 * self.speed:
                        y_when_blocked = cr.bottom + 0
                        up_down_count += 1
                    if dy > 0 and abs(pr.bottom - cr.top) <= 3 * self.speed:
                        y_when_blocked = cr.top - player_cell.height - 0
                        up_down_count += 1
                if left_right_count > 0 and left_right_count == up_down_count:
                    self.map_x_px = x_when_blocked
                    self.map_y_px = y_when_blocked
                elif left_right_count > up_down_count:
                    self.map_x_px = x_when_blocked
                elif up_down_count > left_right_count:
                    self.map_y_px = y_when_blocked

                if self.map_x_px < 0:
                    self.map_x_px = 0
                elif self.map_x_px > len(i_level_matrix[0]) * CELL_SIZE:
                    self.map_x_px = len(i_level_matrix[0]) * CELL_SIZE
                if self.map_y_px < 0:
                    self.map_y_px = 0
                elif self.map_y_px > len(i_level_matrix) * CELL_SIZE:
                    self.map_y_px = len(i_level_matrix) * CELL_SIZE

        elif self.state == PlayerState.meditating:
            if i_btn_just_pressed == input_control.ButtonPressed.btn_b:
                self.state = PlayerState.moving
            else:
                if self.controller.btn_a:
                    self.breathing_width += 0.5
                else:
                    self.breathing_width -= 0.3

        # Here we could test that there is no overlap between the player and collision_cell_list
        # return collision_cell_list

        center_px_x, center_px_y = pr.center
        player_cell_location_x, player_cell_location_y = px_to_cell_coords(center_px_x, center_px_y)
        return (player_cell_location_x, player_cell_location_y)

    def draw(self, i_fake_surface):
        super().draw(i_fake_surface)

        if self.state == PlayerState.meditating:
            max_width = 200
            min_width = 50
            if self.breathing_width > max_width:
                self.breathing_width = max_width
            elif self.breathing_width < min_width:
                self.breathing_width = min_width
            draw_area_rect = pygame.Rect(
                (NR_PX_X - self.breathing_width) // 2, NR_PX_Y - 100,
                self.breathing_width, 15
            )
            pygame.draw.rect(i_fake_surface, WHITE, draw_area_rect)
            # draw_breathing_gui(i_fake_surface, self.breathing_width)


object_representations_dict = {
    '#': WallCell,
    'x': GroundCell,
    'm': MeditatingMonkCell,
    't': TreeCell,
    'T': LargeTree,
    '>': ExitCell,
}

# Fonts:
# https://fontlibrary.org/en/font/retroscape
# https://strlen.com/square/


# controller = input_control.Gamepad(0)
controller = input_control.KeyboardArrows()
player_cell = Player(3, 4, controller)


def main():
    win_surface = pygame.display.set_mode(NORMAL_WIN_SIZE, flags=WIN_FLAGS_NORMAL)
    # -"It is usually best to not pass the depth argument. It will default to the best and
    # fastest color depth for the system."
    fps_clock = pygame.time.Clock()
    fake_surface = win_surface.copy()

    # start_area = Area("levels/level-40x15.lvl")
    start_area = Area("levels/level-40x15.lvl")
    start_area.set_as_active()

    while True:
        event_list: list[pygame.event] = pygame.event.get()
        button_just_pressed = input_control.ButtonPressed.none

        while event_list:
            event = event_list.pop()
            if event.type == pygame.locals.QUIT:
                pygame.quit()
                sys.exit(0)
            elif event.type == pygame.locals.VIDEORESIZE:
                if win_surface.get_flags() == WIN_FLAGS_NORMAL:
                    new_flags = WIN_FLAGS_WITH_FULLSCREEN
                else:
                    new_flags = WIN_FLAGS_NORMAL
                win_surface = pygame.display.set_mode(event.size, flags=new_flags)
            elif event.type == pygame.locals.KEYDOWN:
                if event.key in (pygame.locals.K_q,):
                    pygame.quit()
                    sys.exit(0)
                elif event.key == pygame.locals.K_ESCAPE:
                    win_surface = pygame.display.set_mode(NORMAL_WIN_SIZE, flags=WIN_FLAGS_NORMAL)
                elif event.key == pygame.locals.K_f:
                    # win_surface = pygame.display.toggle_fullscreen()
                    win_surface = pygame.display.set_mode(NORMAL_WIN_SIZE, flags=WIN_FLAGS_WITH_FULLSCREEN)
            button_just_pressed = player_cell.controller.update(event)

        # new_x, new_y, dx, dy = player_cell.get_new_pos_and_direction()
        # player_cell.update()

        fake_surface.fill(DARK_GREEN)

        player_x = player_cell.get_map_rect().x
        player_y = player_cell.get_map_rect().y
        level_cell_length_x = len(Area.active.map_matrix[0])  # -The first line determines the length
        level_cell_length_y = len(Area.active.map_matrix)

        new_camera_x = player_x - NR_PX_X // 2
        new_camera_y = player_y - NR_PX_Y // 2
        if new_camera_x < 0:
            new_camera_x = 0
        elif new_camera_x + NR_PX_X > level_cell_length_x * CELL_SIZE:
            new_camera_x = level_cell_length_x * CELL_SIZE - NR_PX_X
        if new_camera_y < 0:
            new_camera_y = 0
        elif new_camera_y + NR_PX_Y > level_cell_length_y * CELL_SIZE:
            new_camera_y = level_cell_length_y * CELL_SIZE - NR_PX_Y

        Camera.set_pos(new_camera_x, new_camera_y)

        player_cell_location_x, player_cell_location_y = player_cell.update(Area.active.map_matrix, button_just_pressed)
        player_location_cell_obj: GridObject = Area.active.map_matrix[player_cell_location_y][player_cell_location_x]
        if player_location_cell_obj and player_location_cell_obj.when_entered_func:
            player_location_cell_obj.when_entered_func()

        """        
        if player_location_cell_obj and isinstance(player_location_cell_obj, ExitCell):
            new_area = Area("levels/level-2-20x15.lvl")
            new_area.set_as_active()
        """

        for y, level_line in enumerate(Area.active.map_matrix):
            for x in range(len(level_line)):
                cell_obj = level_line[x]
                if cell_obj:
                    cell_obj: GridObject
                    cell_obj.draw(fake_surface)
            # if y == player_cell_location_y:
                # -so that the player is partially hidden if there's an object above

        player_cell.draw(fake_surface)

        # fake_surface = pygame.transform.scale2x(fake_surface)
        # scaled_fake_surface = pygame.transform.scale(fake_surface, (400, 200))
        win_surface.blit(fake_surface, (0, 0))


        # pygame.display.update()
        pygame.display.flip()
        fps_clock.tick(60)


if __name__ == "__main__":
    main()
