import abc
import enum

import pygame
import pygame.locals

"""
        button_pressed_bool = any([self.up, self.down, self.left, self.right])
        return button_pressed_bool

"""


class ButtonPressed(enum.Enum):
    none = enum.auto()
    btn_a = enum.auto()
    btn_b = enum.auto()


class Controller(abc.ABC):
    def __init__(self):
        self.up = False
        self.down = False
        self.left = False
        self.right = False
        self.btn_a = False
        self.btn_b = False

    @abc.abstractmethod
    def get_name(self) -> str:
        pass

    def get_delta_pos(self) -> (int, int):
        delta_x = 0
        delta_y = 0
        if self.left:
            if self.up:
                delta_x = -2
                delta_y = -2
            elif self.down:
                delta_x = -2
                delta_y = 2
            else:
                delta_x = -3
                delta_y = 0
        elif self.right:
            if self.up:
                delta_x = 2
                delta_y = -2
            elif self.down:
                delta_x = 2
                delta_y = 2
            else:
                delta_x = 3
                delta_y = 0
        elif self.up:
            delta_x = 0
            delta_y = -3
        elif self.down:
            delta_x = 0
            delta_y = 3
        return (delta_x, delta_y)

    @abc.abstractmethod
    def update(self, i_event) -> bool:
        pass


class Gamepad(Controller):
    def __init__(self, i_id: int):
        self.pg_joystick = pygame.joystick.Joystick(i_id)
        self.instance_id_int = self.pg_joystick.get_instance_id()
        print(f"{i_id=}")
        print(f"{self.pg_joystick.get_instance_id()=}")
        print(f"{self.pg_joystick.get_name()=}")
        self.pg_joystick.init()

        super().__init__()

    def get_name(self) -> str:
        name_str = self.pg_joystick.get_name()
        return name_str

    def update(self, i_event: pygame.event) -> ButtonPressed:
        # Documentation: https://www.pygame.org/docs/ref/event.html
        if i_event.type not in (pygame.locals.JOYHATMOTION, pygame.JOYAXISMOTION, pygame.locals.JOYBUTTONUP, pygame.locals.JOYBUTTONDOWN):
            return ButtonPressed.none
        if i_event.instance_id != self.pg_joystick.get_instance_id():
            return ButtonPressed.none

        if i_event.type in (pygame.locals.JOYBUTTONDOWN, pygame.locals.JOYBUTTONUP):
            if not self.btn_a and self.pg_joystick.get_button(pygame.CONTROLLER_BUTTON_A):
                self.btn_a = self.pg_joystick.get_button(pygame.CONTROLLER_BUTTON_A)
                return ButtonPressed.btn_a
            self.btn_a = self.pg_joystick.get_button(pygame.CONTROLLER_BUTTON_A)
            if not self.btn_b and self.pg_joystick.get_button(pygame.CONTROLLER_BUTTON_B):
                self.btn_b = self.pg_joystick.get_button(pygame.CONTROLLER_BUTTON_B)
                return ButtonPressed.btn_b
            self.btn_b = self.pg_joystick.get_button(pygame.CONTROLLER_BUTTON_B)

        self.left = False
        self.right = False
        self.up = False
        self.down = False

        # TODO WIP if i_event == self.instance_id_int
        js_hat = self.pg_joystick.get_hat(0)
        # if js_hat:
        if i_event.type == pygame.locals.JOYHATMOTION:
            js_hat_left_right = js_hat[0]
            # TODO: Trying using > and < 0 here (for 8bitdo snes controller)
            if js_hat_left_right == -1:
                self.left = True
            elif js_hat_left_right == 1:
                self.right = True
            js_hat_down_up = js_hat[1]
            if js_hat_down_up == -1:
                self.down = True
            elif js_hat_down_up == 1:
                self.up = True

        if i_event.type == pygame.locals.JOYAXISMOTION:
            js_analog_left_right = self.pg_joystick.get_axis(0)
            if js_analog_left_right < -0.4:
                self.left = True
            elif js_analog_left_right > 0.4:
                self.right = True
            js_analog_up_down = self.pg_joystick.get_axis(1)
            if js_analog_up_down < -0.4:
                self.up = True
            elif js_analog_up_down > 0.4:
                self.down = True

        return ButtonPressed.none


class Keyboard(Controller):
    def __init__(self):
        super().__init__()

    @abc.abstractmethod
    def update(self, i_event) -> bool:
        pass


RETURN_OR_ENTER = (pygame.locals.K_RSHIFT, pygame.locals.K_LSHIFT)
SHIFT = (pygame.locals.K_RSHIFT, pygame.locals.K_LSHIFT)


class KeyboardArrows(Keyboard):
    def __init__(self):
        super().__init__()

    def get_name(self) -> str:
        return "Keyboard directional arrows"

    def update(self, i_event) -> ButtonPressed:
        if i_event.type == pygame.locals.KEYDOWN:
            if i_event.key == pygame.locals.K_LEFT:
                self.left = True
            elif i_event.key == pygame.locals.K_RIGHT:
                self.right = True
            elif i_event.key == pygame.locals.K_UP:
                self.up = True
            elif i_event.key == pygame.locals.K_DOWN:
                self.down = True
            elif i_event.key == pygame.locals.K_RSHIFT:
                if not self.btn_a:
                    self.btn_a = True
                    return ButtonPressed.btn_a
            elif i_event.key in RETURN_OR_ENTER:
                if not self.btn_b:
                    self.btn_b = True
                    return ButtonPressed.btn_b
        elif i_event.type == pygame.locals.KEYUP:
            if i_event.key == pygame.locals.K_LEFT:
                self.left = False
            elif i_event.key == pygame.locals.K_RIGHT:
                self.right = False
            elif i_event.key == pygame.locals.K_UP:
                self.up = False
            elif i_event.key == pygame.locals.K_DOWN:
                self.down = False
            elif i_event.key == pygame.locals.K_RSHIFT:
                self.btn_a = False
            elif i_event.key in RETURN_OR_ENTER:
                self.btn_b = False

        return ButtonPressed.none


class KeyboardWasd(Keyboard):
    def __init__(self):
        super().__init__()

    def get_name(self) -> str:
        return "Keyboard WASD"

    def update(self, i_event):
        if i_event.type == pygame.locals.KEYDOWN:
            if i_event.key == pygame.locals.K_a:
                self.left = True
            elif i_event.key == pygame.locals.K_d:
                self.right = True
            elif i_event.key == pygame.locals.K_w:
                self.up = True
            elif i_event.key == pygame.locals.K_s:
                self.down = True
            elif i_event.key == pygame.locals.K_LSHIFT:
                self.btn_a = True
            elif i_event.key == pygame.locals.K_TAB:
                self.btn_b = True
        elif i_event.type == pygame.locals.KEYUP:
            if i_event.key == pygame.locals.K_a:
                self.left = False
            elif i_event.key == pygame.locals.K_d:
                self.right = False
            elif i_event.key == pygame.locals.K_w:
                self.up = False
            elif i_event.key == pygame.locals.K_s:
                self.down = False
            elif i_event.key == pygame.locals.K_LSHIFT:
                self.btn_a = False
            elif i_event.key == pygame.locals.K_TAB:
                self.btn_b = False
