import sys
import enum
import pygame
import pygame.locals
import input_control

# https://gitlab.com/SunyataZero/robot-cubes/-/blob/master/robotcubes.py
# https://gitlab.com/SunyataZero/stress-umbrella/-/blob/master/backups/blank.py

# Sprites:
# space invaders (pygame example) - see utveckling repo
# https://www.pygame.org/docs/ref/sprite.html
# WIP: https://gitlab.com/SunyataZero/archanoid/-/blob/wip-sprites/coop-arkanoid-pt3.py

# 32x32 pixel art:
# https://opengameart.org/art-search-advanced?field_art_tags_tid=32x32

CELL_SIZE = 32
NR_CELLS_X = 30
NR_CELLS_Y = 20
WIN_WIDTH = NR_CELLS_X * CELL_SIZE
WIN_HEIGHT = NR_CELLS_Y * CELL_SIZE

pygame.init()
win_surface = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT), flags=0, depth=32)
fps_clock = pygame.time.Clock()

GRAY = (128, 128, 128)
WHITE = (225, 225, 225)
BLACK = (30, 30, 30)
GREEN = (30, 230, 30)
BROWN = (130, 50, 50)


# class Direction(enum.Enum):


class GameObject():
    def __init__(self, i_xpx: int, i_ypx, i_width: int, i_height: int):
        self.x = i_xpx
        self.y = i_ypx
        self.width = i_width
        self.height = i_height
        self.color_te = WHITE

    def draw(self):
        # Default, to be overridden
        rectangle = self.get_rect()
        # new_surface = pygame.transform.scale(win_surface, (2*WIN_WIDTH, 2*WIN_HEIGHT))
        pygame.draw.rect(win_surface, self.color_te, rectangle)

    def get_rect(self):
        ret_rect = pygame.Rect(self.x, self.y, self.width, self.height)
        return ret_rect


class Cell(GameObject):
    def __init__(self, i_xc: int, i_yc):
        xpx = CELL_SIZE * i_xc
        ypx = CELL_SIZE * i_yc
        super().__init__(xpx, ypx, CELL_SIZE, CELL_SIZE)
        self.color_te = GRAY


class WallCell(Cell):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.color_te = GREEN


class GroundCell(Cell):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.color_te = BROWN


class GhostCell(Cell):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.image = pygame.image.load("ghost-tedit.png")

    def draw(self):
        rect = self.get_rect()
        surface = pygame.transform.scale(self.image, (self.width*2, self.height*2))
        # .scale, .scale2x, .smoothscale
        win_surface.blit(surface, (rect.x, rect.y))
        # win_surface.blit(self.image, (rect.x, rect.y))


class Player(Cell):
    def __init__(self, i_xc: int, i_yc: int, i_controller):
        super().__init__(i_xc, i_yc)
        self.color_te = WHITE
        self.controller = i_controller
        self.speed = 1
        self.last_nonzero_dx = 0
        self.last_nonzero_dy = 0

    def get_new_pos_and_direction(self) -> (int, int):
        dx, dy = self.controller.get_delta_pos()
        if dx < 0:
            self.last_nonzero_dx = -1
        elif dx > 0:
            self.last_nonzero_dx = 1
        if dy < 0:
            self.last_nonzero_dy = -1
        elif dy > 0:
            self.last_nonzero_dy = 1

        new_x = self.x + dx * self.speed
        new_y = self.y + dy * self.speed
        return (new_x, new_y, dx, dy)

    def set_new_pos(self, i_new_x, i_new_y):
        self.x = i_new_x
        self.y = i_new_y

    def draw(self):
        super().draw()


object_representations_dict = {
    '#': WallCell,
    'x': GroundCell,
    'g': GhostCell
}

# Fonts:
# https://fontlibrary.org/en/font/retroscape
# https://strlen.com/square/

controller = input_control.KeyboardArrows()
player_cell = Player(3, 4, controller)


def load_level():
    ret_level_matrix = [[None for _ in range(NR_CELLS_X)] for _ in range(NR_CELLS_Y)]
    with open("level.lvl", "r") as file:
        line_list = file.readlines()
        for i, line_str in enumerate(line_list):
            for j, char_str in enumerate(line_str.strip()):
                cell_object = None
                if char_str == '@':
                    global player_cell
                    player_cell = Player(j, i, controller)
                elif char_str != ' ':
                    cell_class = object_representations_dict[char_str]
                    cell_object = cell_class(j, i)
                ret_level_matrix[i][j] = cell_object
    return ret_level_matrix


level_matrix = load_level()


while True:
    events: list[pygame.event] = pygame.event.get()
    win_surface.fill(BLACK)

    while events:
        event = events.pop()
        if event.type == pygame.locals.QUIT:
            pygame.quit()
            sys.exit(0)
        # elif event.type == pygame.locals.KEYDOWN:

        player_cell.controller.update(event)

    new_x, new_y, dx, dy = player_cell.get_new_pos_and_direction()
    # player_cell.update()

    for i, level_list in enumerate(level_matrix):
        for j, cell_obj in enumerate(level_list):
            if cell_obj:
                cell_obj: Cell
                cr = cell_obj.get_rect()
                pr = player_cell.get_rect()
                if cr.colliderect(pr):
                    print(f"cr collision {i}, {j}, pixels: {cell_obj.x} {cell_obj.y}")
                    if dx < 0:
                        new_x = cr.right + 2
                    elif dx > 0:
                        new_x = cr.left - player_cell.width - 2

                    elif dy < 0:
                        new_y = cr.bottom + 2
                    elif dy > 0:
                        new_y = cr.top - player_cell.height - 2

                cell_obj.draw()

    player_cell.set_new_pos(new_x, new_y)
    player_cell.draw()

    pygame.display.update()
    fps_clock.tick(30)

