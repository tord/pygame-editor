import sys
import pygame
import pygame.locals
import input_control

# https://gitlab.com/SunyataZero/robot-cubes/-/blob/master/robotcubes.py
# https://gitlab.com/SunyataZero/stress-umbrella/-/blob/master/backups/blank.py

CELL_SIZE = 32
NR_CELLS_X = 30
NR_CELLS_Y = 20
WIN_WIDTH = NR_CELLS_X * CELL_SIZE
WIN_HEIGHT = NR_CELLS_Y * CELL_SIZE

pygame.init()
win_surface = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT), flags=0, depth=32)
pygame.display.set_caption("editor")
fps_clock = pygame.time.Clock()

GRAY = (128, 128, 128)
WHITE = (225, 225, 225)
BLACK = (30, 30, 30)
GREEN = (30, 230, 30)
BROWN = (130, 70, 70)


class GameObject():
    def __init__(self, i_xpx: int, i_ypx, i_width: int, i_height: int):
        self.x = i_xpx
        self.y = i_ypx
        self.width = i_width
        self.height = i_height
        self.color_te = WHITE

    def draw(self):
        # Default, to be overridden
        rectangle = pygame.Rect(self.x, self.y, self.width, self.height)
        pygame.draw.rect(win_surface, self.color_te, rectangle)

    def get_rect(self):
        ret_rect = pygame.Rect(self.x, self.y, self.width, self.height)
        return ret_rect


class Cell(GameObject):
    def __init__(self, i_xc: int, i_yc):
        xpx = CELL_SIZE * i_xc
        ypx = CELL_SIZE * i_yc
        super().__init__(xpx, ypx, CELL_SIZE, CELL_SIZE)
        self.color_te = GRAY

    def get_rect(self):
        ret_rect = pygame.Rect(self.x, self.y, self.width, self.height)
        return ret_rect


class WallCell(Cell):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.color_te = GREEN


class GroundCell(Cell):
    def __init__(self, i_xc: int, i_yc):
        super().__init__(i_xc, i_yc)
        self.color_te = BROWN


class Player(Cell):
    def __init__(self, i_xc: int, i_yc: int, i_controller):
        super().__init__(i_xc, i_yc)
        self.color_te = WHITE
        self.controller = i_controller
        self.speed = 1

    def draw(self):
        (dx, dy) = self.controller.get_delta_pos()
        self.x += dx * self.speed
        if self.x < 0:
            self.x = 0
        if self.x + self.width > WIN_WIDTH:
            self.x = WIN_WIDTH - self.width
        self.y += dy * self.speed
        if self.y < 0:
            self.y = 0
        if self.y + self.height > WIN_HEIGHT:
            self.y = WIN_HEIGHT - self.height

        super().draw()


object_representations_dict = {'#': WallCell, 'x': GroundCell}

controller = input_control.KeyboardArrows()
player_cell = Player(3, 3, controller)


# Fonts:
# https://fontlibrary.org/en/font/retroscape
# https://strlen.com/square/
def load_level():
    ret_level_matrix = [[None for _ in range(NR_CELLS_X)] for _ in range(NR_CELLS_Y)]
    with open("level.lvl", "r") as file:
        line_list = file.readlines()
        for i, line_str in enumerate(line_list):
            for j, char_str in enumerate(line_str.strip()):
                cell_object = None
                if char_str is not ' ':
                    cell_class = object_representations_dict[char_str]
                    cell_object = cell_class(j, i)
                ret_level_matrix[i][j] = cell_object
                """
                if char_str in object_representations_dict.keys():
                    cell_class = object_representations_dict[char_str]
                else:
                    cell_class = Cell
                """
    return ret_level_matrix


# level_file_contents: str = load_level()
level_matrix = load_level()


while True:
    events: list[pygame.event] = pygame.event.get()
    win_surface.fill(BLACK)

    while events:
        event = events.pop()
        if event.type == pygame.locals.QUIT:
            pygame.quit()
            sys.exit(0)
        # elif event.type == pygame.locals.KEYDOWN:

        player_cell.controller.update(event)

    for i, line in enumerate(level_matrix):
        for j, cell_obj in enumerate(line):
            if cell_obj:
                cr = cell_obj.get_rect()
                pr = player_cell.get_rect()
                if cr.colliderect(pr):
                    print(f"cr collision {i}, {j}")

                cell_obj.draw()

    player_cell.draw()

    pygame.display.update()
    fps_clock.tick(30)

